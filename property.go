// Copyright (C) 2014 by krepa098. All rights reserved.
// Use of this source code is governed by a zlib-style
// license that can be found in the license.txt file.

package acropolis

//=======================
//		TYPES
//=======================

type Property struct {
	Data interface{}
}

//=======================
//		FUNCS
//=======================

func (this Property) Bool() bool {
	if val, ok := this.Data.(bool); ok {
		return val
	}
	return false
}

func (this Property) Int() int {
	if val, ok := this.Data.(int); ok {
		return val
	}
	return 0
}

func (this Property) Str() string {
	if val, ok := this.Data.(string); ok {
		return val
	}
	return ""
}
