// Copyright (C) 2014 by krepa098. All rights reserved.
// Use of this source code is governed by a zlib-style
// license that can be found in the license.txt file.

package acropolis

//=======================
//		STRUCTS
//=======================

type SizeConstrain struct {
	max    int
	min    int
	size   int
	fixed  bool
	widget Widgeter
}

type SizeConstrainSlice []SizeConstrain

//=======================
//		FUNCS
//=======================

func (slice SizeConstrainSlice) Sum() (s int) {
	for i := 0; i < len(slice); i++ {
		s += slice[i].size
	}
	return
}

func (slice SizeConstrainSlice) SumMax() (s int) {
	for i := 0; i < len(slice); i++ {
		if !slice[i].fixed {
			s += slice[i].max
		}
	}
	return
}

func (slice SizeConstrainSlice) WidgetData(w Widgeter) SizeConstrain {
	for i := 0; i < len(slice); i++ {
		if slice[i].widget == w {
			return slice[i]
		}
	}
	return SizeConstrain{}
}

func distribute(itemSizeConstrains SizeConstrainSlice, availableSpace int) (SizeConstrainSlice, int) {
	spaceLeft := availableSpace
	done := false

	for !done {
		done = true
		ratio := float32(itemSizeConstrains.SumMax()) / float32(spaceLeft)

		for i := 0; i < len(itemSizeConstrains); i++ {
			if itemSizeConstrains[i].fixed {
				continue
			}

			size := int(float32(itemSizeConstrains[i].max) / ratio)

			if size < itemSizeConstrains[i].min {
				size = itemSizeConstrains[i].min
				spaceLeft -= size
				itemSizeConstrains[i].fixed = true
				done = false
			}

			if size > itemSizeConstrains[i].max {
				size = itemSizeConstrains[i].max
			}

			itemSizeConstrains[i].size = size
		}
	}

	return itemSizeConstrains, availableSpace - itemSizeConstrains.Sum()
}

func distributeWidgets(widgets []Widgeter, horizontal bool, policy int, availableSize int) (SizeConstrainSlice, int) {
	workingConstrains := make(SizeConstrainSlice, 0)

	//filter widgets
	for i := 0; i < len(widgets); i++ {
		if !widgets[i].Visible() {
			continue
		}

		if (widgets[i].SizePolicy().Horizontal == policy && horizontal) || (widgets[i].SizePolicy().Vertical == policy && !horizontal) {
			workingConstrains = append(workingConstrains, SizeConstraintFromWidgeter(widgets[i], horizontal))
		}
	}

	return distribute(workingConstrains, availableSize)
}

func SizeConstraintFromWidgeter(w Widgeter, horizontal bool) (sc SizeConstrain) {
	sc.widget = w

	if horizontal {
		switch w.SizePolicy().Horizontal {
		case Fixed:
			sc.max = w.PreferedSize().Width
			sc.min = w.PreferedSize().Width
		default:
			sc.max = w.MaxSize().Width
			sc.min = w.MinSize().Width
		}
	} else {
		switch w.SizePolicy().Vertical {
		case Fixed:
			sc.max = w.PreferedSize().Height
			sc.min = w.PreferedSize().Height
		default:
			sc.max = w.MaxSize().Height
			sc.min = w.MinSize().Height
		}
	}

	return
}
