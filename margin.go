// Copyright (C) 2014 by krepa098. All rights reserved.
// Use of this source code is governed by a zlib-style
// license that can be found in the license.txt file.

package acropolis

//=======================
//		INTERFACES
//=======================

type Marginer interface {
	Shrink(Geometry) Geometry
	Grow(Geometry) Geometry

	Marshal() map[string]interface{}
	Unmarshal(JsonRawMap)
}

//=======================
//		STRUCTS
//=======================

type AbsMargin struct {
	Left, Top, Right, Bottom int
}

type RelMargin struct {
	Left, Top, Right, Bottom float32
}

//impl. check
var _ Marginer = &AbsMargin{}
var _ Marginer = &RelMargin{}

//=======================
//		FUNCS
//=======================

func (margin AbsMargin) Shrink(geom Geometry) Geometry {
	return Geometry{geom.Left + margin.Left, geom.Top + margin.Top, geom.Width - margin.Right - margin.Left, geom.Height - margin.Bottom - margin.Top}
}

func (margin RelMargin) Shrink(geom Geometry) Geometry {
	return Geometry{
		geom.Left + int(float32(geom.Width)*margin.Left),
		geom.Top + int(float32(geom.Height)*margin.Top),
		geom.Width - int(float32(geom.Width)*margin.Right) - int(float32(geom.Width)*margin.Left),
		geom.Height - int(float32(geom.Height)*margin.Bottom) - int(float32(geom.Height)*margin.Top)}
}

func (margin AbsMargin) Grow(geom Geometry) Geometry {
	return Geometry{geom.Left - margin.Left, geom.Top - margin.Top, geom.Width + margin.Right + margin.Left, geom.Height + margin.Bottom + margin.Top}
}

func (margin RelMargin) Grow(geom Geometry) Geometry {
	return Geometry{
		geom.Left - int(float32(geom.Width)*margin.Left),
		geom.Top - int(float32(geom.Height)*margin.Top),
		geom.Width + int(float32(geom.Width)*margin.Right) + int(float32(geom.Width)*margin.Left),
		geom.Height + int(float32(geom.Height)*margin.Bottom) + int(float32(geom.Height)*margin.Top)}
}

func (this RelMargin) Marshal() map[string]interface{} {
	return map[string]interface{}{
		"Left":   this.Left,
		"Right":  this.Right,
		"Top":    this.Top,
		"Bottom": this.Bottom,
	}
}

func (this *RelMargin) Unmarshal(data JsonRawMap) {
	Unmarshal(data["Left"], &this.Left)
	Unmarshal(data["Right"], &this.Right)
	Unmarshal(data["Top"], &this.Top)
	Unmarshal(data["Bottom"], &this.Bottom)
}

func (this AbsMargin) Marshal() map[string]interface{} {
	return map[string]interface{}{
		"Left":   this.Left,
		"Right":  this.Right,
		"Top":    this.Top,
		"Bottom": this.Bottom,
	}
}

func (this *AbsMargin) Unmarshal(data JsonRawMap) {
	Unmarshal(data["Left"], &this.Left)
	Unmarshal(data["Right"], &this.Right)
	Unmarshal(data["Top"], &this.Top)
	Unmarshal(data["Bottom"], &this.Bottom)
}
