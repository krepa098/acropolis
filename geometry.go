// Copyright (C) 2014 by krepa098. All rights reserved.
// Use of this source code is governed by a zlib-style
// license that can be found in the license.txt file.

package acropolis

//=======================
//		STRUCTS
//=======================

type Geometry struct {
	Left, Top, Width, Height int
}

type Size struct {
	Width, Height int
}

//=======================
//		FUNCS
//=======================

func (this Geometry) IsPointInside(x, y int) bool {
	return this.Left <= x && this.Left+this.Width >= x &&
		this.Top <= y && this.Top+this.Height >= y
}

func (this Geometry) Shrink(margin Marginer) Geometry {
	if margin == nil {
		return this
	}

	return margin.Shrink(this)
}

func (this Geometry) Grow(margin Marginer) Geometry {
	if margin == nil {
		return this
	}

	return margin.Grow(this)
}

func (this Geometry) Move(x, y int) Geometry {
	this.Left += x
	this.Top += y
	return this
}

func (this Geometry) Equals(other Geometry) bool {
	return this.Left == other.Left && this.Top == other.Top && this.Width == other.Width && this.Height == other.Height
}

func (this Geometry) Size() Size {
	return Size{this.Width, this.Height}
}

func (this *Size) SetMax() {
	this.Width = 16777216 //2^24
	this.Height = 16777216
}

func (this Size) Equals(other Size) bool {
	return this.Width == other.Width && this.Height == other.Height
}
