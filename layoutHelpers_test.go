// Copyright (C) 2014 by krepa098. All rights reserved.
// Use of this source code is governed by a zlib-style
// license that can be found in the license.txt file.

package acropolis

import "fmt"

//=======================
//		FUNCS
//=======================

func DistributeWidgets(space int, constrains ...SizeConstrain) {
	newSpaces, restSpace := distribute(constrains, space)

	for i := 0; i < len(newSpaces); i++ {
		fmt.Print(" ", newSpaces[i].size)
	}
	fmt.Println("")
	fmt.Println(restSpace)
}

//=======================
//		EXAMPLES
//=======================

func Example_WD1() {
	DistributeWidgets(100, SizeConstrain{max: 25, min: 0}, SizeConstrain{max: 50, min: 0})
	// Output:
	// 25 50
	// 25
}

func Example_WD2() {
	DistributeWidgets(100, SizeConstrain{max: 100, min: 0}, SizeConstrain{max: 50, min: 0})
	// Output:
	// 66 33
	// 1
}

func Example_WD3() {
	DistributeWidgets(100, SizeConstrain{max: 100, min: 75}, SizeConstrain{max: 50, min: 25})
	// Output:
	// 75 25
	// 0
}

func Example_WD4() {
	DistributeWidgets(100, SizeConstrain{max: 100, min: 75}, SizeConstrain{max: 150, min: 15})
	// Output:
	// 75 25
	// 0
}

func Example_WD5() {
	DistributeWidgets(100, SizeConstrain{max: 100, min: 100}, SizeConstrain{max: 150, min: 15})
	// Output:
	// 100 15
	// -15
}

func Example_WD6() {
	DistributeWidgets(100, SizeConstrain{max: 50, min: 25}, SizeConstrain{max: 25, min: 0})
	// Output:
	// 50 25
	// 25
}

func Example_WD7() {
	DistributeWidgets(100, SizeConstrain{max: 50, min: 25}, SizeConstrain{max: 25, min: 0}, SizeConstrain{max: 50, min: 25})
	// Output:
	// 40 20 40
	// 0
}

func Example_WD8() {
	DistributeWidgets(100, SizeConstrain{max: 150, min: 25})
	// Output:
	// 100
	// 0
}

func Example_WD9() {
	DistributeWidgets(300, SizeConstrain{max: 100, min: 25}, SizeConstrain{max: 100, min: 50}, SizeConstrain{max: 100, min: 25})
	// Output:
	// 100 100 100
	// 0
}

func Example_WD10() {
	DistributeWidgets(300, SizeConstrain{max: 30, min: 30}, SizeConstrain{max: 200, min: 0}, SizeConstrain{max: 200, min: 0})
	// Output:
	// 30 135 135
	// 0
}

func Example_WD11() {
	DistributeWidgets(100)
	// Output:
	// 100
}

func Example_WD12() {
	DistributeWidgets(100, SizeConstrain{max: 150, min: 150})
	// Output:
	// 150
	// -50
}

func Example_WD13() {
	DistributeWidgets(100, SizeConstrain{max: 50, min: 50})
	// Output:
	// 50
	// 50
}

func Example_WD14() {
	DistributeWidgets(100, SizeConstrain{max: 0, min: 0}, SizeConstrain{max: 50, min: 50})
	// Output:
	// 0 50
	// 50
}

func Example_WD15() {
	DistributeWidgets(100, SizeConstrain{max: 150, min: 150}, SizeConstrain{max: 150, min: 150})
	// Output:
	// 150 150
	// -200
}
