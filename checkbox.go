// Copyright (C) 2014 by krepa098. All rights reserved.
// Use of this source code is governed by a zlib-style
// license that can be found in the license.txt file.

package acropolis

//=======================
//		STRUCTS
//=======================

type Checkbox struct {
	WidgetBase
	Painter PainterFunc
	Checked bool

	//events
	OnEvent EventFunc
}

var _ Widgeter = &Checkbox{}

//=======================
//		FUNCS
//=======================

func NewCheckbox() *Checkbox {
	widget := &Checkbox{}
	widget.SetName(GenerateWidgetName(widget))
	widget.SetVisible(true)
	widget.maxSize.SetMax()
	return widget
}

func (this *Checkbox) CheckGeometry() Geometry {
	return this.Geometry()
}

func (this *Checkbox) Draw(painter PainterFunc, renderContext RenderContext, userData interface{}) {
	if renderContext != DefaultContext || !this.visible {
		return
	}

	painter(this, renderContext, userData)
}

func (this *Checkbox) Event(ev Event) (Widgeter, Widgeter) {
	if ev.IsPositionDependant() && !this.geometry.IsPointInside(ev.X, ev.Y) {
		return nil, nil
	}

	if this.OnEvent != nil {
		this.OnEvent(this, ev)
	}

	// Handle check/uncheck
	if ev.Type == MouseReleased {
		if this.CheckGeometry().IsPointInside(ev.X, ev.Y) {
			if this.Checked {
				this.Checked = false
			} else {
				this.Checked = true
			}
		}
	}

	return this, nil
}

func (this *Checkbox) Marshal() map[string]interface{} {
	m := this.WidgetBase.Marshal()
	m["Checked"] = this.Checked

	return m
}

func (this *Checkbox) Unmarshal(data JsonRawMap) {
	this.WidgetBase.Unmarshal(data)
	Unmarshal(data["Checked"], &this.Checked)
}
