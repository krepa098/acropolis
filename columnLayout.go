// Copyright (C) 2014 by krepa098. All rights reserved.
// Use of this source code is governed by a zlib-style
// license that can be found in the license.txt file.

package acropolis

//=======================
//		STRUCTS
//=======================

type ColumnLayout struct {
	parent  WidgetContainer
	spacing int
	align   int
}

var _ Layouter = &ColumnLayout{} //implementation check

//=======================
//		FUNCS
//=======================

func (this *ColumnLayout) Attach(parent WidgetContainer) {
	this.parent = parent
}

func (this *ColumnLayout) InsertWidget(widget Widgeter) {
	if this.parent == nil {
		return
	}

	this.parent.AddChild(widget)
}

func (this *ColumnLayout) SetSpacing(spacing int) {
	this.spacing = spacing
	this.RepositionElements()
}

func (this *ColumnLayout) SetAlignment(align int) {
	this.align = align
	this.RepositionElements()
}

func (this *ColumnLayout) RepositionElements() {
	if this.parent == nil {
		return
	}

	children := this.parent.Children()
	geom := this.parent.ContentGeometry()
	geom.Width -= this.spacing * (len(children) - 1)

	if len(children) == 0 {
		return
	}

	var data SizeConstrainSlice

	minExpSpace := 0
	fixedSpace := 0
	for i := 0; i < len(children); i++ {
		switch children[i].SizePolicy().Horizontal {
		case MinExpand:
			minExpSpace += children[i].MinSize().Width
		case Fixed:
			fixedSpace += children[i].MinSize().Width
		}
	}

	distributionData, restSpace := distributeWidgets(children, true, Fixed, geom.Width)
	data = append(data, distributionData...)

	distributionData, restSpace = distributeWidgets(children, true, Expand, restSpace-minExpSpace)
	data = append(data, distributionData...)

	distributionData, restSpace = distributeWidgets(children, true, MinExpand, restSpace)
	data = append(data, distributionData...)

	left := geom.Left

	for i := 0; i < len(children); i++ {
		if !children[i].Visible() {
			continue
		}

		d := data.WidgetData(children[i])
		widgetGeom := children[i].Geometry()

		widgetGeom.Left = left
		widgetGeom.Width = d.size

		switch children[i].SizePolicy().Vertical {
		case Expand, MinExpand:
			widgetGeom.Height = geom.Height
		}

		//alignment
		switch this.align {
		case Top:
			widgetGeom.Top = geom.Top
		case Bottom:
			widgetGeom.Top = geom.Top + geom.Height - widgetGeom.Height
		case Center:
			widgetGeom.Top = geom.Top + (geom.Height-widgetGeom.Height)/2
		}

		left += widgetGeom.Width + this.spacing

		children[i].SetGeometry(widgetGeom)
	}
}

func (this *ColumnLayout) MinSize() (size Size) {
	if this.parent == nil {
		return
	}

	children := this.parent.Children()

	//elements
	for _, e := range children {
		size.Height = max(size.Height, e.MinSize().Height)
		size.Width += e.MinSize().Width
	}

	//spacing and margin
	size.Width += this.spacing*len(children) - 1

	size.Width += this.parent.Geometry().Width - this.parent.ContentGeometry().Width
	size.Height += this.parent.Geometry().Height - this.parent.ContentGeometry().Height

	return
}

func (this *ColumnLayout) Marshal() map[string]interface{} {
	return map[string]interface{}{
		"Spacing": this.spacing,
		"Align":   this.align,
	}
}

func (this *ColumnLayout) Unmarshal(data JsonRawMap) {
	Unmarshal(data["Spacing"], &this.spacing)
	Unmarshal(data["Align"], &this.align)
}
