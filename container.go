// Copyright (C) 2014 by krepa098. All rights reserved.
// Use of this source code is governed by a zlib-style
// license that can be found in the license.txt file.

package acropolis

//=======================
//		INTERFACES
//=======================

type Container interface {
	AddChild(child Widgeter)
	Children() []Widgeter
	FindChildren(name string, recursive bool) []Widgeter
	ContentGeometry() Geometry
	Update()
}

type WidgetContainer interface {
	Widgeter
	Container
}
