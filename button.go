// Copyright (C) 2014 by krepa098. All rights reserved.
// Use of this source code is governed by a zlib-style
// license that can be found in the license.txt file.

package acropolis

//=======================
//		STRUCTS
//=======================

type Button struct {
	WidgetBase
	Text  string
	state int
}

var _ Widgeter = &Button{}

//=======================
//		CONSTS
//=======================

const (
	ButtonClicked = CustomEvent + iota
)

const (
	ButtonReleased = iota
	ButtonHoldDown
	ButtonHovered
)

//=======================
//		FUNCS
//=======================

func NewButton() *Button {
	widget := &Button{}
	widget.SetName(GenerateWidgetName(widget))
	widget.SetVisible(true)
	widget.maxSize.SetMax()
	return widget
}

func (this *Button) Draw(painter PainterFunc, renderContext RenderContext, userData interface{}) {
	if renderContext != DefaultContext || !this.visible {
		return
	}

	painter(this, renderContext, userData)
}

func (this *Button) Event(ev Event) (Widgeter, Widgeter) {
	if ev.Type == MouseEntered {
		this.state = ButtonHovered
	}

	if ev.Type == MouseLeft {
		this.state = ButtonReleased
	}

	if ev.IsPositionDependant() && !this.geometry.IsPointInside(ev.X, ev.Y) {
		return nil, nil
	}

	this.callOnEvent(this, ev)

	if ev.Type == MousePressed {
		this.state = ButtonHoldDown
	}

	if ev.Type == MouseReleased {
		this.callOnEvent(this, Event{Type: ButtonClicked})
	}

	return this, nil
}

func (this *Button) State() int {
	return this.state
}

func (this *Button) Marshal() map[string]interface{} {
	m := this.WidgetBase.Marshal()
	m["Text"] = this.Text

	return m
}

func (this *Button) Unmarshal(data JsonRawMap) {
	this.WidgetBase.Unmarshal(data)
	Unmarshal(data["Text"], &this.Text)
}
