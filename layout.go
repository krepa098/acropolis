// Copyright (C) 2014 by krepa098. All rights reserved.
// Use of this source code is governed by a zlib-style
// license that can be found in the license.txt file.

package acropolis

//=======================
//		CONSTS
//=======================

//size policy
const (
	Fixed = iota
	MinExpand
	Expand
)

//align (horizontal)
const (
	//horizontal
	Left = iota
	Center
	Right

	//vertical
	Top    = Left
	Bottom = Right
)

//=======================
//		STRUCTS
//=======================

type SizePolicy struct {
	Horizontal, Vertical int
}

func (this SizePolicy) Equals(other SizePolicy) bool {
	return this.Horizontal == other.Horizontal && this.Vertical == other.Vertical
}

//=======================
//		INTERFACES
//=======================

type Layouter interface {
	Serializer
	RepositionElements()
	Attach(parent WidgetContainer) //called by SetLayout()
	MinSize() Size
}
