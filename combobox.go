// Copyright (C) 2014 by krepa098. All rights reserved.
// Use of this source code is governed by a zlib-style
// license that can be found in the license.txt file.

package acropolis

//=======================
//		STRUCTS
//=======================

type Combobox struct {
	WidgetBase
	DropdownWidget *ComboboxDropdown
	VisibleItems   int

	sliderSize           int
	items                []string
	currentIndex         int
	hoveredItem          int
	mouseReleaseRequired bool
}

var _ Widgeter = &Combobox{}

//=======================
//		CONSTS
//=======================

const (
	ComboboxItemSelected EventType = CustomEvent + iota
	ComboboxOpened
	ComboboxClosed
)

const (
	ComboboxDropdownContext = CustomContext + iota //dropdown menu is visible
)

//=======================
//		FUNCS
//=======================

func NewCombobox() *Combobox {
	widget := &Combobox{}
	widget.SetName(GenerateWidgetName(widget))
	widget.SetVisible(true)
	widget.maxSize.SetMax()
	widget.VisibleItems = 5
	widget.sliderSize = 10

	return widget
}

func (this *Combobox) SetItems(items []string) {
	this.items = make([]string, len(items))
	copy(this.items, items)
}

func (this *Combobox) CurrentItem() string {
	if this.currentIndex >= 0 && this.currentIndex < len(this.items) {
		return this.items[this.currentIndex]
	}
	return "error"
}

func (this *Combobox) SetCurrentItem(index int) {
	if index >= 0 && index < this.ItemCount() {
		this.currentIndex = index
	}
}

func (this *Combobox) Draw(painter PainterFunc, renderContext RenderContext, userData interface{}) {
	if !this.visible {
		return
	}

	painter(this, renderContext, userData)
}

func (this *Combobox) Event(ev Event) (Widgeter, Widgeter) {
	if ev.Type == MousePressed {
		this.mouseReleaseRequired = true
		this.OpenDropdown()
	}

	if ev.Type == Close {
		this.CloseDropdown()
	}

	if ev.Type == MouseReleased && this.mouseReleaseRequired {
		this.mouseReleaseRequired = false
	}

	return this, this.DropdownWidget
}

func (this *Combobox) OpenDropdown() {
	this.DropdownWidget = NewComboboxDropdown(this.DropdownGeometry(), this.Geometry().Height, this.items, this.sliderSize)
	this.DropdownWidget.OnItemSelected = func(index int) {
		this.currentIndex = index
		this.CloseDropdown()
	}
}

func (this *Combobox) CloseDropdown() {
	if this.DropdownWidget == nil {
		return
	}

	this.DropdownWidget.Close()
	this.DropdownWidget = nil
}

func (this *Combobox) DropdownGeometry() Geometry {
	return this.Geometry().Move(0, this.Geometry().Height).Grow(&AbsMargin{Bottom: min(len(this.items)-1, this.VisibleItems) * this.Geometry().Height})
}

func (this *Combobox) DropdownContentGeometry() Geometry {
	return this.Geometry().Move(0, this.Geometry().Height).Grow(&AbsMargin{Bottom: (len(this.items) - 1) * this.Geometry().Height})
}

func (this *Combobox) ItemCount() int {
	return len(this.items)
}

func (this *Combobox) Item(index int) string {
	if index >= 0 && index < this.ItemCount() {
		return this.items[index]
	}
	return ""
}

func (this *Combobox) Marshal() map[string]interface{} {
	m := this.WidgetBase.Marshal()
	m["Items"] = this.items
	m["VisibleItems"] = this.VisibleItems
	m["SliderSize"] = this.sliderSize

	return m
}

func (this *Combobox) Unmarshal(data JsonRawMap) {
	this.WidgetBase.Unmarshal(data)
	Unmarshal(data["Items"], &this.items)
	Unmarshal(data["VisibleItems"], &this.VisibleItems)
	Unmarshal(data["SliderSize"], &this.sliderSize)
}
