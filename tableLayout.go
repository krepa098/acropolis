// Copyright (C) 2014 by krepa098. All rights reserved.
// Use of this source code is governed by a zlib-style
// license that can be found in the license.txt file.

package acropolis

//=======================
//		STRUCTS
//=======================

type TableLayout struct {
	parent           WidgetContainer
	children         [][]string //rows,cols
	spacing          int
	rows, columns    int
	columnWidthRatio []float32
}

var _ Layouter = &TableLayout{} //implementation check

//=======================
//		FUNCS
//=======================

func (this *TableLayout) Attach(parent WidgetContainer) {
	this.parent = parent
}

func (this *TableLayout) SetDimensions(rows, columns int) {
	this.rows, this.columns = rows, columns

	this.children = make([][]string, rows)
	for i := 0; i < len(this.children); i++ {
		this.children[i] = make([]string, columns)
	}

	this.columnWidthRatio = make([]float32, columns)
	for i := 0; i < columns; i++ {
		this.columnWidthRatio[i] = 1 / float32(columns)
	}
}

func (this *TableLayout) InsertWidget(row, column int, widget Widgeter) {
	if this.parent == nil {
		return
	}

	if row < 0 || row >= this.rows || column < 0 || column >= this.columns {
		return
	}

	if this.children[row][column] != "" {
		panic("only one widget per cell is accepted")
	}

	this.children[row][column] = widget.Name()
	this.parent.AddChild(widget)
}

func (this *TableLayout) SetSpacing(spacing int) {
	this.spacing = spacing
	this.RepositionElements()
}

func (this *TableLayout) RepositionElements() {
	if this.parent == nil {
		return
	}

	children := this.parent.Children()
	geom := this.parent.ContentGeometry()

	if this == nil || len(children) == 0 {
		return
	}

	rowHeight := geom.Height/this.rows - this.spacing
	top := geom.Top

	for r := 0; r < this.rows; r++ {

		h := this.rowHeight(geom)
		for c := 0; c < this.columns; c++ {
			//set geometry
			widget := this.child(this.children[r][c])
			if widget == nil || !widget.Visible() {
				continue
			}

			left := this.columnLeftPoint(geom, c)
			w := 0

			switch widget.SizePolicy().Vertical {
			case Fixed:
				h = widget.MinSize().Height
			default:
				h = rowHeight
			}

			switch widget.SizePolicy().Horizontal {
			case Fixed:
				w = widget.MinSize().Width
			default:
				w = this.columnWidth(geom, c)
			}

			widgetGeom := Geometry{left, top, w, h}

			widget.SetGeometry(widgetGeom)
		}
		top += h + this.spacing
	}
}

func (this *TableLayout) child(name string) Widgeter {
	w := this.parent.FindChildren(name, false)
	if len(w) > 0 {
		return w[0]
	}
	return nil
}

func (this *TableLayout) MinSize() (size Size) {
	if this.parent == nil {
		return
	}

	//height
	for r := 0; r < this.rows; r++ {
		h := 0
		for c := 0; c < this.columns; c++ {
			widget := this.child(this.children[r][c])
			if widget == nil {
				continue
			}

			h = max(widget.MinSize().Height, h)

		}
		size.Height += h
	}

	//width
	for c := 0; c < this.columns; c++ {
		w := 0
		for r := 0; r < this.rows; r++ {
			widget := this.child(this.children[r][c])
			if widget == nil {
				continue
			}

			w = max(widget.Geometry().Width, w)
		}
		size.Width += w
	}

	//margins
	geom := this.parent.ContentGeometry()
	size.Height += this.parent.Geometry().Height - geom.Height
	size.Width += this.parent.Geometry().Width - geom.Width

	//spacing
	size.Height += this.spacing * (this.rows - 1)

	return
}

func (this *TableLayout) rowHeight(geom Geometry) (h int) {
	return geom.Height / this.rows
}

func (this *TableLayout) SetColumnWidthRatios(width ...float32) {
	if len(width) != this.columns {
		panic("SetComumnWidth: Dimensions do not match.")
	}

	//normalize
	s := float32(0.0)
	for i := 0; i < len(width); i++ {
		s += width[i]
	}

	for i := 0; i < len(width); i++ {
		width[i] /= s
	}

	this.columnWidthRatio = width
}

func (this *TableLayout) columnLeftPoint(geom Geometry, col int) int {
	s := geom.Left
	for i := 0; i < col; i++ {
		s += int(float32(geom.Width) * this.columnWidthRatio[i])
	}
	return s
}

func (this *TableLayout) columnWidth(geom Geometry, col int) int {
	return int(float32(geom.Width) * this.columnWidthRatio[col])
}

func (this *TableLayout) Marshal() map[string]interface{} {
	return map[string]interface{}{
		"Spacing":          this.spacing,
		"Elements":         this.children,
		"Rows":             this.rows,
		"Columns":          this.columns,
		"ColumnWithRatios": this.columnWidthRatio,
	}
}

func (this *TableLayout) Unmarshal(data JsonRawMap) {
	Unmarshal(data["Spacing"], &this.spacing)

	Unmarshal(data["Rows"], &this.rows)
	Unmarshal(data["Columns"], &this.columns)

	this.SetDimensions(this.rows, this.columns)
	Unmarshal(data["Elements"], &this.children)
	Unmarshal(data["ColumnWithRatios"], &this.columnWidthRatio)
}
