// Copyright (C) 2014 by krepa098. All rights reserved.
// Use of this source code is governed by a zlib-style
// license that can be found in the license.txt file.

package acropolis

import (
	"encoding/json"
	"io/ioutil"
	"os"
)

//=======================
//		STRUCTS
//=======================

type Manager struct {
	screen *Screen

	focusedWidget Widgeter
	popupWidget   Widgeter
	hoveredWidget Widgeter
	clickedWidget Widgeter
	widgetlock    bool
}

//=======================
//		FUNCS
//=======================

//creates a new gui manager and an empty screen
func NewManager() *Manager {
	return &Manager{screen: NewScreen()}
}

func (this *Manager) Draw(Painter PainterFunc, userData interface{}) {
	if this.screen == nil || Painter == nil {
		return
	}

	this.screen.Draw(Painter, DefaultContext, userData)

	if this.popupWidget != nil {
		this.popupWidget.Draw(Painter, DefaultContext, userData)
	}
}

func (this *Manager) Screen() *Screen {
	return this.screen
}

func (this *Manager) SetScreen(screen *Screen) {
	this.screen = screen
}

func (this *Manager) FeedEvent(ev Event) {
	if this.screen == nil {
		return
	}

	//all events to this widget
	if this.clickedWidget != nil {
		this.clickedWidget.Event(ev)
		if ev.Type == MouseReleased {
			this.clickedWidget = nil
		}

		return
	}

	//popup widget
	if this.popupWidget != nil {
		if (ev.Type == MouseMoved || ev.Type == MouseWheelMoved) && !this.popupWidget.Geometry().IsPointInside(ev.X, ev.Y) {
			return
		}

		//every mouse press needs a mouse release on the same widget
		if ev.Type == MousePressed {
			this.clickedWidget = this.popupWidget
		}

		//close when clicking outsite the popup
		if ev.IsClickEvent() && !this.popupWidget.Geometry().IsPointInside(ev.X, ev.Y) {
			this.popupWidget.Event(Event{Type: Close})

			this.popupWidget = nil
			return
		}
		
		//process event
		this.popupWidget, _ = this.popupWidget.Event(ev)

		return
	}

	//focused widget
	if this.focusedWidget != nil && ev.IsExclusive() {
		this.focusedWidget.Event(ev)
		return
	}

	//
	if ev.Type == MousePressed {
		widget, popup := this.screen.Event(ev)

		this.focusedWidget = widget
		this.clickedWidget = widget

		if popup != nil {
			this.popupWidget = popup
		}

		return
	}

	//hovering
	if ev.Type == MouseMoved {
		widget, _ := this.screen.Event(ev)
		if this.hoveredWidget != widget {
			if widget != nil {
				widget.Event(Event{Type: MouseEntered})
			}

			if this.hoveredWidget != nil {
				this.hoveredWidget.Event(Event{Type: MouseLeft})
			}
		}

		this.hoveredWidget = widget

		return
	}

	//standard event forwarding
	this.screen.Event(ev)
}

func (this *Manager) SaveScreen(file string) {
	data, _ := json.MarshalIndent(Marshal(this.screen), "", "	")
	ioutil.WriteFile(file, data, os.ModePerm)
}

func LoadScreen(file string) (screen *Screen, err error) {
	data, err := ioutil.ReadFile(file)

	if err != nil {
		return nil, err
	}

	var unmarshaledData JsonRawMap
	json.Unmarshal(data, &unmarshaledData)

	screen = NewScreen()
	screen.Unmarshal(unmarshaledData)

	return
}
