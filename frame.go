// Copyright (C) 2014 by krepa098. All rights reserved.
// Use of this source code is governed by a zlib-style
// license that can be found in the license.txt file.

package acropolis

//=======================
//		STRUCTS
//=======================

type Frame struct {
	WidgetContainerBase
	Painter PainterFunc

	layout Layouter
	margin Marginer
}

var _ WidgetContainer = &Frame{}

//=======================
//		FUNCS
//=======================

func NewFrame() *Frame {
	widget := &Frame{}
	widget.SetName(GenerateWidgetName(widget))
	widget.SetVisible(true)
	widget.maxSize.SetMax()
	widget.SetSizePolicy(SizePolicy{MinExpand, MinExpand})
	return widget
}

func (this *Frame) Draw(painter PainterFunc, renderContext RenderContext, userData interface{}) {
	if !this.visible {
		return
	}

	painter(this, ClipContext, userData)
	painter(this, renderContext, userData)

	if this.Painter != nil {
		this.Painter(this, renderContext, userData)
	}

	for _, e := range this.Children() {
		e.Draw(painter, renderContext, userData)
	}

	painter(this, UnclipContext, userData)
}

func (this *Frame) SetVisible(visible bool) {
	this.visible = visible
	this.Update()
}

func (this *Frame) ContentGeometry() Geometry {
	return this.geometry.Shrink(this.margin)
}

func (this *Frame) SetLayout(layout Layouter) {
	this.layout = layout

	if this.layout != nil {
		this.layout.Attach(this)
	}

	this.Update()
}

func (this *Frame) Layout() Layouter {
	return this.layout
}

func (this *Frame) MinSize() Size {
	if !this.visible {
		return Size{}
	}

	if this.layout != nil {
		return this.layout.MinSize()
	}

	return this.minSize
}

func (this *Frame) Update() {
	if this.parent != nil {
		this.parent.Update()
	}

	if this.layout != nil {
		this.layout.RepositionElements()
	}
}

func (this *Frame) SetMargin(margin Marginer) {
	this.margin = margin
	if this.layout != nil {
		this.layout.RepositionElements()
	}
}

func (this *Frame) SetGeometry(geometry Geometry) {
	if !this.geometry.Equals(geometry) {

		this.geometry = geometry

		if this.layout != nil {
			this.layout.RepositionElements()
		}
	}
}

func (this *Frame) AddChild(child Widgeter) {
	this.WidgetContainerBase.AddChild(child)
	child.SetParent(this)
	if this.layout != nil {
		this.layout.RepositionElements()
	}
}

func (this *Frame) Event(ev Event) (Widgeter, Widgeter) {
	//self
	if ev.IsPositionDependant() && !this.Geometry().IsPointInside(ev.X, ev.Y) {
		return nil, nil
	}

	this.callOnEvent(this, ev)

	//forward
	if !ev.IsExclusive() {
		for _, e := range this.Children() {
			if e.Visible() { //hidden widgets do not receive events
				if ev.IsPositionDependant() && !e.Geometry().IsPointInside(ev.X, ev.Y) {
					continue
				}

				w, a := e.Event(ev)
				if w != nil {
					return w, a
				}
			}
		}
	}

	return nil, nil //frame is not focusable
}

func (this *Frame) Marshal() map[string]interface{} {
	m := this.WidgetBase.Marshal()
	m["Layout"] = Marshal(this.layout)
	m["Margin"] = Marshal(this.margin)
	m["Children"] = Marshal(this.children)

	return m
}

func (this *Frame) Unmarshal(data JsonRawMap) {
	this.WidgetBase.Unmarshal(data)
	Unmarshal(data["Layout"], &this.layout)
	Unmarshal(data["Margin"], &this.margin)

	if this.layout != nil {
		this.layout.Attach(this)
	}

	children := UnmarshalArray(data["Children"])
	for i := 0; i < len(children); i++ {
		if _, ok := children[i].(Widgeter); ok {
			this.AddChild(children[i].(Widgeter))
		}
	}
}
