// Copyright (C) 2014 by krepa098. All rights reserved.
// Use of this source code is governed by a zlib-style
// license that can be found in the license.txt file.

package acropolis

//=======================
//		STRUCTS
//=======================

type Lineedit struct {
	WidgetBase
	Text          string
	textCursorPos int
}

var _ Widgeter = &Label{}

//=======================
//		FUNCS
//=======================

func NewLineedit() *Lineedit {
	widget := &Lineedit{}
	widget.SetName(GenerateWidgetName(widget))
	widget.SetVisible(true)
	widget.maxSize.SetMax()
	return widget
}

func (this *Lineedit) Draw(painter PainterFunc, renderContext RenderContext, userData interface{}) {
	if renderContext != DefaultContext || !this.visible {
		return
	}

	painter(this, renderContext, userData)
}

func (this *Lineedit) Event(ev Event) (Widgeter, Widgeter) {
	if ev.IsPositionDependant() && !this.geometry.IsPointInside(ev.X, ev.Y) {
		return nil, nil
	}

	//user event
	this.callOnEvent(this, ev)

	//text editing
	if ev.Type == KeyPressed {
		//backspace
		if ev.Back && len(this.Text) > 0 && this.textCursorPos > 0 {
			this.Text = this.Text[:this.textCursorPos-1] + this.Text[this.textCursorPos:]
			this.textCursorPos--
		}

		//delete
		if ev.Delete && len(this.Text) > 0 && this.textCursorPos < len(this.Text) {
			this.Text = this.Text[:this.textCursorPos] + this.Text[this.textCursorPos+1:]
		}

		//left
		if ev.Left && this.textCursorPos > 0 {
			this.textCursorPos--
		}

		//right
		if ev.Right && this.textCursorPos < len(this.Text) {
			this.textCursorPos++
		}
	}

	//text input
	if ev.Type == TextEntered {
		this.Text = this.Text[:this.textCursorPos] + string(ev.Text) + this.Text[this.textCursorPos:]
		this.textCursorPos++
	}

	return this, nil
}

func (this *Lineedit) CursorPosition() int {
	return this.textCursorPos
}

func (this *Lineedit) SetCursorPosition(pos int) {
	this.textCursorPos = min(max(pos, 0), len(this.Text)-1)
}

func (this *Lineedit) Marshal() map[string]interface{} {
	m := this.WidgetBase.Marshal()
	m["Text"] = this.Text

	return m
}

func (this *Lineedit) Unmarshal(data JsonRawMap) {
	this.WidgetBase.Unmarshal(data)
	Unmarshal(data["Text"], &this.Text)
}
