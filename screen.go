// Copyright (C) 2014 by krepa098. All rights reserved.
// Use of this source code is governed by a zlib-style
// license that can be found in the license.txt file.

package acropolis

//=======================
//		STRUCTS
//=======================

type Screen struct {
	WidgetContainerBase
	Painter PainterFunc

	layout Layouter
	margin Marginer
}

var _ WidgetContainer = &Screen{}

//=======================
//		FUNCS
//=======================

func NewScreen() *Screen {
	screen := &Screen{}
	screen.SetName("root")
	screen.SetVisible(true)
	screen.maxSize.SetMax()
	screen.SetSizePolicy(SizePolicy{Expand, Expand})
	return screen
}

func (this *Screen) Draw(painter PainterFunc, renderContext RenderContext, userData interface{}) {
	for _, e := range this.Children() {
		e.Draw(painter, renderContext, userData)
	}
}

func (this *Screen) ContentGeometry() Geometry {
	return this.geometry.Shrink(this.margin)
}

func (this *Screen) SetLayout(layout Layouter) {
	this.layout = layout

	if this.layout != nil {
		this.layout.Attach(this)
		this.layout.RepositionElements()
	}
}

func (this *Screen) Layout() Layouter {
	return this.layout
}

func (this *Screen) MinSize() Size {
	if !this.visible {
		return Size{}
	}

	if this.layout != nil {
		return this.layout.MinSize()
	}

	return this.minSize
}

func (this *Screen) SetMargin(margin Marginer) {
	this.margin = margin
	if this.layout != nil {
		this.layout.RepositionElements()
	}
}

func (this *Screen) SetGeometry(geometry Geometry) {
	if !this.geometry.Equals(geometry) {

		this.geometry = geometry

		if this.layout != nil {
			this.layout.RepositionElements()
		}
	}
}

func (this *Screen) AddChild(child Widgeter) {
	this.WidgetContainerBase.AddChild(child)
	child.SetParent(this)
	if this.layout != nil {
		this.layout.RepositionElements()
	}
}

func (this *Screen) Event(ev Event) (Widgeter, Widgeter) {
	//forward
	if !ev.IsExclusive() {
		for _, e := range this.Children() {
			if e.Visible() { //hidden widgets do not receive events
				if ev.IsPositionDependant() && !e.Geometry().IsPointInside(ev.X, ev.Y) {
					continue
				}

				w, a := e.Event(ev)
				if w != nil {
					return w, a
				}
			}
		}
	}

	return nil, nil
}

func (this *Screen) Marshal() map[string]interface{} {
	m := this.WidgetBase.Marshal()
	m["Layout"] = Marshal(this.layout)
	m["Margin"] = Marshal(this.margin)
	m["Children"] = Marshal(this.children)

	return m
}

func (this *Screen) Unmarshal(data JsonRawMap) {
	this.WidgetBase.Unmarshal(data)
	Unmarshal(data["Layout"], &this.layout)
	Unmarshal(data["Margin"], &this.margin)

	if this.layout != nil {
		this.layout.Attach(this)
	}

	children := UnmarshalArray(data["Children"])
	for i := 0; i < len(children); i++ {
		if _, ok := children[i].(Widgeter); ok {
			this.AddChild(children[i].(Widgeter))
		}
	}
}
