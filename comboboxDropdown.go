// Copyright (C) 2014 by krepa098. All rights reserved.
// Use of this source code is governed by a zlib-style
// license that can be found in the license.txt file.

package acropolis

import "strconv"

//=======================
//		STRUCTS
//=======================

type ComboboxDropdown struct {
	WidgetBase
	scrollArea     *ScrollArea
	VisibleItems   int
	OnItemSelected func(int)

	items        []string
	currentIndex int
	hoveredItem  int
}

var _ Widgeter = &ComboboxDropdown{}

//=======================
//		FUNCS
//=======================

func NewComboboxDropdown(geom Geometry, itemHeight int, items []string, sliderSize int) *ComboboxDropdown {
	w := &ComboboxDropdown{}

	w.scrollArea = NewScrollArea()
	w.scrollArea.SetGeometry(geom)
	w.scrollArea.SetLayout(&RowLayout{})
	w.scrollArea.SetVisible(true)
	w.scrollArea.SetSliderSize(sliderSize)

	//fill scrollArea with items
	for i, s := range items {
		labelWidget := NewLabel()
		labelWidget.name = strconv.Itoa(i)
		labelWidget.Text = s
		labelWidget.SetSizePolicy(SizePolicy{Expand, Fixed})
		labelWidget.SetPreferedSize(Size{0, itemHeight})
		labelWidget.SetMinSize(Size{0, itemHeight})
		w.scrollArea.Layout().(*RowLayout).InsertWidget(labelWidget)
	}
	w.scrollArea.Layout().RepositionElements()
	w.visible = true

	return w
}

func (this *ComboboxDropdown) Geometry() Geometry {
	return this.scrollArea.Geometry()
}

func (this *ComboboxDropdown) Draw(painter PainterFunc, renderContext RenderContext, userData interface{}) {
	if !this.visible {
		return
	}

	this.scrollArea.Draw(painter, renderContext, userData)
}

func (this *ComboboxDropdown) Event(ev Event) (Widgeter, Widgeter) {
	if ev.Type == Close {
		this.Close()
		return nil, nil //close this widget
	}

	w, _ := this.scrollArea.Event(ev)

	//item selection
	if w != nil && ev.Type == MousePressed {
		index, err := strconv.Atoi(w.Name())
		if this.OnItemSelected != nil && err == nil {
			this.OnItemSelected(index)
			this.Close()
			return nil, nil //close this widget
		}
	}

	return this, nil
}

func (this *ComboboxDropdown) Close() {
	this.SetVisible(false)
}

func (this *ComboboxDropdown) Open() {
	this.SetVisible(true)
}

func (this *ComboboxDropdown) indexFromPosition(y int) int {
	if y < this.Geometry().Top {
		return this.indexFromPosition(this.Geometry().Top)
	}

	if y > this.Geometry().Top+this.Geometry().Height {
		return this.indexFromPosition(this.Geometry().Top + this.Geometry().Height - 1)
	}

	return clamp((y-this.scrollArea.ContentGeometry().Top)/this.Geometry().Height, 0, len(this.items))
}

func (this *ComboboxDropdown) Marshal() map[string]interface{} {
	return map[string]interface{}{}
}

func (this *ComboboxDropdown) Unmarshal(data JsonRawMap) {

}
