// Copyright (C) 2014 by krepa098. All rights reserved.
// Use of this source code is governed by a zlib-style
// license that can be found in the license.txt file.

package acropolis

import "strconv"

//=======================
//		STRUCTS
//=======================

type Spinbox struct {
	WidgetBase
	SpinDownArea GeomFunc //Area of the spindown button (absolute coords)
	SpinUpArea   GeomFunc //Area of the spinup button (absolute coords)
	items        []string
	currentIndex int
}

var _ Widgeter = &Spinbox{}

//=======================
//		FUNCS
//=======================

func NewSpinbox() *Spinbox {
	widget := &Spinbox{}
	widget.SetName(GenerateWidgetName(widget))
	widget.SetVisible(true)
	widget.maxSize.SetMax()
	return widget
}

func (this *Spinbox) SetInterval(from, to, step int) {
	if from > to {
		return
	}

	this.items = make([]string, 0, (to-from)/step+2)
	for i := from; i < to; i += step {
		this.items = append(this.items, strconv.Itoa(i))
	}
	this.items = append(this.items, strconv.Itoa(to))
}

func (this *Spinbox) SetItems(items []string) {
	this.items = make([]string, len(items))
	copy(this.items, items)
}

func (this *Spinbox) CurrentItem() string {
	if this.currentIndex >= 0 && this.currentIndex < len(this.items) {
		return this.items[this.currentIndex]
	}
	return "---"
}

func (this *Spinbox) SpindownGeometry() Geometry {
	w := this.Geometry().Height
	return Geometry{this.Geometry().Left + this.Geometry().Width - w - w, this.Geometry().Top, w, this.Geometry().Height}
}

func (this *Spinbox) SpinupGeometry() Geometry {
	w := this.Geometry().Height
	return Geometry{this.Geometry().Left + this.Geometry().Width - w, this.Geometry().Top, w, this.Geometry().Height}
}

func (this *Spinbox) Draw(painter PainterFunc, renderContext RenderContext, userData interface{}) {
	if renderContext != DefaultContext || !this.visible {
		return
	}

	painter(this, renderContext, userData)
}

func (this *Spinbox) Event(ev Event) (Widgeter, Widgeter) {
	if ev.IsPositionDependant() && !this.Geometry().IsPointInside(ev.X, ev.Y) {
		return nil, nil
	}

	this.callOnEvent(this, ev)

	// Handle spinup & spindown events
	if ev.Type == MousePressed {
		if this.SpindownGeometry().IsPointInside(ev.X, ev.Y) {
			if this.currentIndex-1 >= 0 {
				this.currentIndex--
			}
		}

		if this.SpinupGeometry().IsPointInside(ev.X, ev.Y) {
			if this.currentIndex+1 < len(this.items) {
				this.currentIndex++
			}
		}
	}

	return this, nil
}

func (this *Spinbox) Marshal() map[string]interface{} {
	m := this.WidgetBase.Marshal()
	m["CurrentIndex"] = this.currentIndex
	m["Items"] = this.items

	return m
}

func (this *Spinbox) Unmarshal(data JsonRawMap) {
	this.WidgetBase.Unmarshal(data)
	Unmarshal(data["CurrentIndex"], &this.currentIndex)
	Unmarshal(data["Items"], &this.items)
}
