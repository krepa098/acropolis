// Copyright (C) 2014 by krepa098. All rights reserved.
// Use of this source code is governed by a zlib-style
// license that can be found in the license.txt file.

package acropolis

//=======================
//		STRUCTS
//=======================

type ScrollArea struct {
	WidgetContainerBase

	layout       Layouter
	margin       Marginer
	staticMargin Marginer

	Hscroll, Vscroll int
	sliderSize       int

	offsetX, offsetY int
	vsliderPressed   bool
	hsliderPressed   bool
}

var _ WidgetContainer = &ScrollArea{}

//=======================
//		FUNCS
//=======================

func NewScrollArea() *ScrollArea {
	widget := &ScrollArea{}
	widget.SetName(GenerateWidgetName(widget))
	widget.SetVisible(true)
	widget.maxSize.SetMax()
	widget.SetSizePolicy(SizePolicy{MinExpand, MinExpand})
	widget.SetSliderSize(10)

	return widget
}

func (this *ScrollArea) Contentpane() Geometry {
	if this.layout != nil {
		return Geometry{this.Geometry().Left, this.Geometry().Top, this.MinSize().Width, this.MinSize().Height}
	}

	return Geometry{}
}

func (this *ScrollArea) Oversize() (s Size) {
	if this.Contentpane().Width > this.geometry.Width {
		s.Width = this.Contentpane().Width - this.geometry.Width
	}

	if this.Contentpane().Height > this.geometry.Height {
		s.Height = this.Contentpane().Height - this.geometry.Height
	}

	return
}

func (this *ScrollArea) HSliderVisible() bool {
	return this.Oversize().Width > 0

}

func (this *ScrollArea) VSliderVisible() bool {
	return this.Oversize().Height > 0
}

func (this *ScrollArea) ScrollH(delta int) {
	if this.Oversize().Width > 0 {
		this.Hscroll = clamp(this.Hscroll-delta, 0, this.Oversize().Width)
		this.layout.RepositionElements()
	}
}

func (this *ScrollArea) ScrollV(delta int) {
	if this.Oversize().Height > 0 {
		this.Vscroll = clamp(this.Vscroll-delta, 0, this.Oversize().Height)
		this.layout.RepositionElements()
	}
}

func (this *ScrollArea) ResetScrolling() {
	this.Hscroll, this.Vscroll = 0, 0
}

func (this *ScrollArea) SetSliderSize(size int) {
	this.sliderSize = size
	//update margin
	this.margin = &AbsMargin{0, 0, size, size}
	this.Update()
}

func (this *ScrollArea) vsliderHeight() int {
	return max(this.Geometry().Height-this.Oversize().Height, this.sliderSize)
}

func (this *ScrollArea) hsliderWidth() int {
	return max(this.Geometry().Width-this.Oversize().Width, this.sliderSize)
}

func (this *ScrollArea) HSliderGeometry() Geometry {
	r := float32(this.geometry.Width) / float32(this.Oversize().Width+this.hsliderWidth())
	return Geometry{this.geometry.Left + int(float32(this.Hscroll)*r), this.geometry.Top + this.geometry.Height - this.sliderSize, this.hsliderWidth(), this.sliderSize}
}

func (this *ScrollArea) VSliderGeometry() Geometry {
	r := float32(this.geometry.Height) / float32(this.Oversize().Height+this.vsliderHeight())
	return Geometry{this.geometry.Left + this.geometry.Width - this.sliderSize, this.geometry.Top + int(float32(this.Vscroll)*r), this.sliderSize, this.vsliderHeight()}
}

func (this *ScrollArea) Event(ev Event) (Widgeter, Widgeter) {
	switch ev.Type {
	case MousePressed:
		//vslider pressed?
		if this.VSliderVisible() && this.VSliderGeometry().IsPointInside(ev.X, ev.Y) {
			this.vsliderPressed = true
			this.offsetX = ev.X - this.VSliderGeometry().Left
			this.offsetY = ev.Y - this.VSliderGeometry().Top
			return this, nil
		}

		//hslider pressed?
		if this.HSliderVisible() && this.HSliderGeometry().IsPointInside(ev.X, ev.Y) {
			this.hsliderPressed = true
			this.offsetX = ev.X - this.HSliderGeometry().Left
			this.offsetY = ev.Y - this.HSliderGeometry().Top
			return this, nil
		}

	case MouseReleased:
		//release both sliders
		this.vsliderPressed = false
		this.hsliderPressed = false
		return this, nil

	case MouseMoved:
		//vslider scrolling
		if this.vsliderPressed {
			delta := this.VSliderGeometry().Top - ev.Y + this.offsetY
			this.ScrollV(delta)
		}
		//hslider scrolling
		if this.hsliderPressed {
			delta := this.HSliderGeometry().Left - ev.X + this.offsetX
			this.ScrollH(delta)
		}
		return this, nil
	case MouseWheelMoved:
		this.ScrollV(ev.Delta * this.Geometry().Height/4)
	}

	//forward
	if !ev.IsExclusive() {
		for _, e := range this.Children() {
			if e.Visible() { //hidden widgets do not receive events
				if ev.IsPositionDependant() && !e.Geometry().IsPointInside(ev.X, ev.Y) {
					continue
				}

				w, a := e.Event(ev)
				if w != nil {
					return w, a
				}
			}
		}
	}

	return nil, nil
}

func (this *ScrollArea) Draw(painter PainterFunc, renderContext RenderContext, userData interface{}) {
	if !this.visible {
		return
	}

	painter(this, ClipContext, userData)
	painter(this, renderContext, userData)

	for _, e := range this.Children() {
		e.Draw(painter, renderContext, userData)
	}
	painter(this, UnclipContext, userData)
}

func (this *ScrollArea) ContentGeometry() Geometry {
	return this.geometry.Shrink(this.margin).Move(-this.Hscroll, -this.Vscroll)
}

func (this *ScrollArea) SetLayout(layout Layouter) {
	this.layout = layout

	if this.layout != nil {
		this.layout.Attach(this)
		this.layout.RepositionElements()
	}
}

func (this *ScrollArea) Layout() Layouter {
	return this.layout
}

func (this *ScrollArea) MinSize() Size {
	if !this.visible {
		return Size{}
	}

	if this.layout != nil {
		return this.layout.MinSize()
	}

	return this.minSize
}

func (this *ScrollArea) SetGeometry(geometry Geometry) {
	if !this.geometry.Equals(geometry) {
		this.geometry = geometry

		if this.layout != nil {
			this.layout.RepositionElements()
		}
	}
}

func (this *ScrollArea) AddChild(child Widgeter) {
	this.WidgetContainerBase.AddChild(child)
	child.SetParent(this)
	if this.layout != nil {
		this.layout.RepositionElements()
	}
}

func (this *ScrollArea) Marshal() map[string]interface{} {
	m := this.WidgetContainerBase.Marshal()
	m["SliderSize"] = this.sliderSize
	m["Margin"] = Marshal(this.margin)
	m["Layout"] = Marshal(this.layout)

	return m
}

func (this *ScrollArea) Unmarshal(data JsonRawMap) {
	this.WidgetContainerBase.Unmarshal(data)

	Unmarshal(data["SliderSize"], &this.sliderSize)
	Unmarshal(data["Layout"], &this.layout)
	Unmarshal(data["Margin"], &this.margin)

	if this.layout != nil {
		this.layout.Attach(this)
	}
}
