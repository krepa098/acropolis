// Copyright (C) 2014 by krepa098. All rights reserved.
// Use of this source code is governed by a zlib-style
// license that can be found in the license.txt file.

package acropolis

const (
	//position dependent events
	MousePressed EventType = 0 + iota
	MouseReleased
	MouseMoved
	MouseWheelMoved
)
const (
	//exclusive events
	TextEntered EventType = 100 + iota
	KeyPressed
	KeyReleased
	MouseEntered
	MouseLeft
	GainedFocus
	LostFocus

	//events created by layouts
	GeometryChanged
	Close
)

const (
	CustomEvent EventType = 200
)

type EventType int

type Event struct {
	Type   EventType
	X, Y   int
	Button int
	Text   rune
	//special keys
	Back, Delete bool
	Left, Right  bool
	Delta        int
}

func (this *Event) IsPositionDependant() bool {
	return this.Type >= 0 && this.Type < 100
}

func (this *Event) IsClickEvent() bool {
	return this.Type == MousePressed || this.Type == MouseReleased
}

func (this *Event) IsExclusive() bool {
	return this.Type >= 100
}

func (this *Event) IsFocusingEvent() bool {
	return this.Type == MouseReleased
}
