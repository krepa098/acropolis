// Copyright (C) 2014 by krepa098. All rights reserved.
// Use of this source code is governed by a zlib-style
// license that can be found in the license.txt file.

package acropolis

//=======================
//		STRUCTS
//=======================

type Label struct {
	WidgetBase
	Text  string
	Align int

	//events
	OnEvent EventFunc
}

var _ Widgeter = &Label{}

//=======================
//		FUNCS
//=======================

func NewLabel() *Label {
	widget := &Label{}
	widget.SetName(GenerateWidgetName(widget))
	widget.SetVisible(true)
	widget.maxSize.SetMax()
	return widget
}

func (this *Label) Draw(painter PainterFunc, renderContext RenderContext, userData interface{}) {
	if !this.visible {
		return
	}

	painter(this, renderContext, userData)
}

func (this *Label) Event(ev Event) (Widgeter, Widgeter) {
	if ev.IsPositionDependant() && !this.geometry.IsPointInside(ev.X, ev.Y) {
		return nil, nil
	}

	if ev.IsClickEvent() {
		if this.OnEvent != nil {
			this.OnEvent(this, ev)
		}

		return this, nil
	}

	return nil, nil
}

func (this *Label) Marshal() map[string]interface{} {
	m := this.WidgetBase.Marshal()
	m["Text"] = this.Text
	m["Align"] = this.Align

	return m
}

func (this *Label) Unmarshal(data JsonRawMap) {
	this.WidgetBase.Unmarshal(data)
	Unmarshal(data["Text"], &this.Text)
	Unmarshal(data["Align"], &this.Align)
}
