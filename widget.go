// Copyright (C) 2014 by krepa098. All rights reserved.
// Use of this source code is governed by a zlib-style
// license that can be found in the license.txt file.

package acropolis

//=======================
//		CONST
//=======================

const (
	DefaultContext RenderContext = iota
	FocusedContext
	ClipContext
	UnclipContext
	CustomContext
)

type RenderContext int

//=======================
//		INTERFACES
//=======================

type Widgeter interface {
	Serializer
	Namer
	Geometry() Geometry
	SetGeometry(Geometry)
	SizePolicy() SizePolicy
	SetSizePolicy(SizePolicy)
	Event(Event) (Widgeter, Widgeter)
	MinSize() Size
	MaxSize() Size
	PreferedSize() Size
	Draw(PainterFunc, RenderContext, interface{})
	SetVisible(bool)
	Visible() bool
	Parent() WidgetContainer
	SetParent(WidgetContainer)
}

//=======================
//		TYPES
//=======================

type PainterFunc func(object Widgeter, context RenderContext, userData interface{})
type EventFunc func(sender Widgeter, ev Event) bool
type GeomFunc func(geom Geometry) Geometry
