// Copyright (C) 2014 by krepa098. All rights reserved.
// Use of this source code is governed by a zlib-style
// license that can be found in the license.txt file.

package acropolis

//=======================
//		STRUCTS
//=======================

type VSpacer struct {
	WidgetBase
}
type HSpacer struct {
	WidgetBase
}

var _ Widgeter = &VSpacer{}
var _ Widgeter = &HSpacer{}

//=======================
//		FUNCS
//=======================

func NewVSpacer() *VSpacer {
	widget := &VSpacer{}
	widget.SetName(GenerateWidgetName(widget))
	widget.visible = true
	widget.maxSize = Size{0, 16777216}
	widget.sizePolicy = SizePolicy{Horizontal: Fixed, Vertical: Expand}
	return widget
}

func (this *VSpacer) Marshal() map[string]interface{} {
	return this.WidgetBase.Marshal()
}

func (this *VSpacer) Unmarshal(data JsonRawMap) {
	this.WidgetBase.Unmarshal(data)
}

//=======================================

func NewHSpacer() *HSpacer {
	widget := &HSpacer{}
	widget.SetName(GenerateWidgetName(widget))
	widget.visible = true
	widget.maxSize = Size{16777216, 0}
	widget.sizePolicy = SizePolicy{Horizontal: Expand, Vertical: Fixed}
	return widget
}

func (this *HSpacer) Marshal() map[string]interface{} {
	return this.WidgetBase.Marshal()
}

func (this *HSpacer) Unmarshal(data JsonRawMap) {
	this.WidgetBase.Unmarshal(data)
}
