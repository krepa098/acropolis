// Copyright (C) 2014 by krepa098. All rights reserved.
// Use of this source code is governed by a zlib-style
// license that can be found in the license.txt file.

package acropolis

//=======================
//		STRUCTS
//=======================

type WidgetBase struct {
	parent       WidgetContainer
	geometry     Geometry
	sizePolicy   SizePolicy
	minSize      Size
	maxSize      Size
	preferedSize Size
	name         string
	visible      bool
	UserData     interface{}
	OnEvent      EventFunc
	Properties   map[string]Property
}

var _ Widgeter = &WidgetBase{}

//=======================
//		FUNCS
//=======================

func (this *WidgetBase) Draw(PainterFunc, RenderContext, interface{}) {

}

func (this *WidgetBase) Event(Event) (Widgeter, Widgeter) {
	return nil, nil
}

func (this *WidgetBase) Geometry() Geometry {
	if this.sizePolicy.Vertical == Fixed {
		this.geometry.Height = this.preferedSize.Height
	}
	if this.sizePolicy.Horizontal == Fixed {
		this.geometry.Width = this.preferedSize.Width
	}

	return this.geometry
}

func (this *WidgetBase) SetGeometry(geom Geometry) {
	if !geom.Equals(this.geometry) {
		this.geometry = geom
	}
}

func (this *WidgetBase) MinSize() (s Size) {
	return this.minSize
}

func (this *WidgetBase) MaxSize() (s Size) {
	return this.maxSize
}

func (this *WidgetBase) PreferedSize() (s Size) {
	return this.preferedSize
}

func (this *WidgetBase) SetMinSize(s Size) {
	if !s.Equals(this.minSize) {
		this.minSize = s
	}
}

func (this *WidgetBase) SetMaxSize(s Size) {
	if !s.Equals(this.maxSize) {
		this.maxSize = s
	}
}

func (this *WidgetBase) SetPreferedSize(s Size) {
	if !s.Equals(this.preferedSize) {
		this.preferedSize = s
	}
}

func (this *WidgetBase) Visible() bool {
	return this.visible
}

func (this *WidgetBase) SetVisible(visible bool) {
	if visible != this.visible {
		this.visible = visible
	}
}

func (this *WidgetBase) SizePolicy() SizePolicy {
	return this.sizePolicy
}

func (this *WidgetBase) SetSizePolicy(policy SizePolicy) {
	if !this.sizePolicy.Equals(policy) {
		this.sizePolicy = policy
	}
}

func (this *WidgetBase) Parent() WidgetContainer {
	return this.parent
}

func (this *WidgetBase) SetParent(c WidgetContainer) {
	if this.parent != c {
		this.parent = c
	}
}

func (this *WidgetBase) Name() string {
	return this.name
}

func (this *WidgetBase) SetName(name string) {
	this.name = name
}

func (this *WidgetBase) SetProperty(name string, data interface{}) {
	if this.Properties == nil {
		this.Properties = make(map[string]Property)
	}

	this.Properties[name] = Property{data}
}

func (this *WidgetBase) Property(name string) Property {
	return this.Properties[name]
}

func (this *WidgetBase) Marshal() map[string]interface{} {
	return map[string]interface{}{
		"MaxSize":      this.maxSize,
		"MinSize":      this.minSize,
		"PreferedSize": this.preferedSize,
		"Geometry":     this.geometry,
		"SizePolicy":   this.sizePolicy,
		"Visible":      this.visible,
		"Name":         this.name,
		"Properties":   this.Properties,
	}
}

func (this *WidgetBase) Unmarshal(data JsonRawMap) {
	Unmarshal(data["SizePolicy"], &this.sizePolicy)
	Unmarshal(data["MaxSize"], &this.maxSize)
	Unmarshal(data["MinSize"], &this.minSize)
	Unmarshal(data["PreferedSize"], &this.preferedSize)
	Unmarshal(data["Geometry"], &this.geometry)
	Unmarshal(data["Visible"], &this.visible)
	Unmarshal(data["Name"], &this.name)
	Unmarshal(data["Properties"], &this.Properties)
}

func (this *WidgetBase) callOnEvent(sender Widgeter, ev Event) {
	if this.OnEvent != nil {
		this.OnEvent(sender, ev)
	}
}
