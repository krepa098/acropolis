// Copyright (C) 2014 by krepa098. All rights reserved.
// Use of this source code is governed by a zlib-style
// license that can be found in the license.txt file.

package acropolis

import (
	"encoding/json"
	"errors"
	"reflect"
	"strconv"
)

func init() {
	//register types for unmarshaling
	//container
	RegisterType(Screen{})
	RegisterType(Frame{})
	RegisterType(ScrollArea{})

	//widgets
	RegisterType(Label{})
	RegisterType(Checkbox{})
	RegisterType(Button{})
	RegisterType(VSpacer{})
	RegisterType(HSpacer{})
	RegisterType(Spinbox{})
	RegisterType(Lineedit{})
	RegisterType(Combobox{})

	//layouts
	RegisterType(RowLayout{})
	RegisterType(ColumnLayout{})
	RegisterType(TableLayout{})
	RegisterType(HudLayout{})

	//margins
	RegisterType(RelMargin{})
	RegisterType(AbsMargin{})
}

//=======================
//		JsonRawMap
//=======================

type JsonRawMap map[string]json.RawMessage

func (this JsonRawMap) GetType() string {
	var objType string
	json.Unmarshal([]byte(this[typeFieldIdentifier]), &objType)
	return objType
}

//=======================
//		JsonArray
//=======================

type JsonArray []interface{}

//=======================
//		Name generation
//=======================

var uids = make(map[string]bool)

func GenerateWidgetName(obj interface{}) (out string) {
	if obj != nil {
		out += typeName(obj) + "_"
	} else {
		out += "uid"
	}

	i := 1
	for _, exists := uids[out+strconv.Itoa(i)]; exists; _, exists = uids[out+strconv.Itoa(i)] {
		i++
	}

	out += strconv.Itoa(i)
	uids[out] = true
	return
}

//=======================
//		INTERFACES
//=======================

type Serializer interface {
	Marshal() map[string]interface{}
	Unmarshal(JsonRawMap)
}

type Namer interface {
	Name() string
	SetName(string)
}

//=======================
//		Marshaling
//=======================

const typeFieldIdentifier = "Type"

func Marshal(v interface{}) interface{} {
	if v == nil {
		return nil
	}

	//marshal array (e.g. for children)
	if reflect.TypeOf(v).Kind() == reflect.Slice {
		var array = make(JsonArray, reflect.ValueOf(v).Len())
		for i := 0; i < reflect.ValueOf(v).Len(); i++ {
			e := reflect.ValueOf(v).Index(i).Interface()
			c, ok := e.(Serializer)
			if ok {
				m := c.Marshal()
				//add it as a field to the json map
				m[typeFieldIdentifier] = typeName(e)
				array[i] = m
			} else {
				array[i] = e
			}
		}
		return array
	}

	//marshal single interface
	serializer, ok := v.(Serializer)
	if ok {
		m := serializer.Marshal()
		//add it as a field to the json map
		m[typeFieldIdentifier] = typeName(v)
		return m
	}
	return v
}

//=======================
//Unmarshaling &reflection
//=======================

var registeredTypes = make(map[string]reflect.Type)

func typeName(i interface{}) string {
	return reflect.Indirect(reflect.ValueOf(i)).Type().String()
}

func RegisterType(i interface{}) {
	registeredTypes[typeName(i)] = reflect.TypeOf(i)
}

func prototype(name string) interface{} {
	if e, ok := registeredTypes[name]; ok {
		return reflect.New(e).Interface()
	}
	return nil
}

func UnmarshalArray(data json.RawMessage) (out []interface{}) {
	var jsonArray []json.RawMessage

	err := json.Unmarshal(data, &jsonArray)
	if err == nil {
		for i := 0; i < len(jsonArray); i++ {
			var obj interface{}
			Unmarshal(jsonArray[i], &obj)
			out = append(out, obj)
		}
		return
	}
	return nil
}

func Unmarshal(data json.RawMessage, o interface{}) error {
	if reflect.TypeOf(o).Kind() != reflect.Ptr {
		return errors.New("Unmarshal: Non-pointer " + reflect.TypeOf(o).String())
	}

	var objData JsonRawMap
	var obj interface{}
	var err error

	if err, obj = json.Unmarshal(data, &objData), prototype(objData.GetType()); err != nil || obj == nil {
		//unknown type or unspecified type
		json.Unmarshal(data, &o)
		return nil
	}

	//call custom unmarshal method if supported
	if serializer, ok := obj.(Serializer); ok {
		serializer.Unmarshal(objData)
	}

	//return
	iv := reflect.Indirect(reflect.ValueOf(o))
	iv.Set(reflect.ValueOf(obj))

	return nil
}
