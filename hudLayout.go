// Copyright (C) 2014 by krepa098. All rights reserved.
// Use of this source code is governed by a zlib-style
// license that can be found in the license.txt file.

package acropolis

//=======================
//		STRUCTS
//=======================

type HudLayout struct {
	parent   WidgetContainer
	children []hudData
	margin   Marginer
}

type hudData struct {
	WidgetName       string
	HorizontalAnchor int
	VerticalAnchor   int
	RelPosX          float32
	RelPosY          float32
	RelSizeX         float32
	AspectRatio      float32
}

var _ Layouter = &HudLayout{} //implementation check

//=======================
//		FUNCS
//=======================

func (this *HudLayout) Attach(parent WidgetContainer) {
	this.parent = parent
}

func (this *HudLayout) InsertWidget(widget Widgeter, hAnchor, vAnchor int, posX, posY, sizeX, aspectRatio float32) {
	if this.parent == nil {
		return
	}

	this.children = append(this.children, hudData{widget.Name(), hAnchor, vAnchor, posX, posY, sizeX, aspectRatio})
	this.parent.AddChild(widget)

}

func (this *HudLayout) SetMargin(margin Marginer) {
	this.margin = margin
	this.RepositionElements()
}

func (this *HudLayout) RepositionElements() {
	if this.parent == nil {
		return
	}

	geom := this.parent.Geometry().Shrink(this.margin)

	for _, c := range this.children {
		w := this.child(c.WidgetName)

		if w == nil {
			continue
		}

		widgetGeom := Geometry{}

		switch w.SizePolicy().Horizontal {
		case Fixed:
			widgetGeom.Width = w.PreferedSize().Width
		default:
			widgetGeom.Width = int(float32(geom.Width) * c.RelSizeX)
		}

		switch w.SizePolicy().Vertical {
		case Fixed:
			widgetGeom.Height = w.PreferedSize().Height
		default:
			widgetGeom.Height = int(float32(widgetGeom.Width) * c.AspectRatio)
		}

		switch c.HorizontalAnchor {
		case Left:
			widgetGeom.Left = geom.Left + int(float32(geom.Width)*c.RelPosX)
		case Right:
			widgetGeom.Left = geom.Left + int(float32(geom.Width)*c.RelPosX) - widgetGeom.Width
		case Center:
			widgetGeom.Left = geom.Left + int(float32(geom.Width)*c.RelPosX) - widgetGeom.Width/2
		}

		switch c.VerticalAnchor {
		case Top:
			widgetGeom.Top = geom.Top + int(float32(geom.Height)*c.RelPosY)
		case Bottom:
			widgetGeom.Top = geom.Top + int(float32(geom.Height)*c.RelPosY) - widgetGeom.Height
		case Center:
			widgetGeom.Top = geom.Top + int(float32(geom.Height)*c.RelPosY) - widgetGeom.Height/2
		}

		w.SetGeometry(widgetGeom)
	}
}

func (this *HudLayout) child(name string) Widgeter {
	if this.parent != nil {
		w := this.parent.FindChildren(name, false)
		if len(w) > 0 {
			return w[0]
		}
	}

	return nil
}

func (this *HudLayout) MinSize() (size Size) {
	if this.parent == nil {
		return
	}

	return this.parent.Geometry().Size()
}

func (this *HudLayout) Marshal() map[string]interface{} {
	return map[string]interface{}{
		"Margin":   Marshal(this.margin),
		"Elements": Marshal(this.children),
	}
}

func (this *HudLayout) Unmarshal(data JsonRawMap) {
	Unmarshal(data["Margin"], &this.margin)
	Unmarshal(data["Elements"], &this.children)
}
