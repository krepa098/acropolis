// Copyright (C) 2014 by krepa098. All rights reserved.
// Use of this source code is governed by a zlib-style
// license that can be found in the license.txt file.

package acropolis

//=======================
//		STRUCTS
//=======================

type RowLayout struct {
	parent  WidgetContainer
	spacing int
	align   int
}

var _ Layouter = &RowLayout{} //implementation check

//=======================
//		FUNCS
//=======================

func (this *RowLayout) Attach(parent WidgetContainer) {
	this.parent = parent
}

func (this *RowLayout) InsertWidget(widget Widgeter) {
	if this.parent == nil {
		return
	}

	this.parent.AddChild(widget)
	this.RepositionElements()
}

func (this *RowLayout) SetSpacing(spacing int) {
	this.spacing = spacing
	this.RepositionElements()
}

func (this *RowLayout) SetAlignment(align int) {
	this.align = align
	this.RepositionElements()
}

func (this *RowLayout) RepositionElements() {
	if this.parent == nil {
		return
	}

	children := this.parent.Children()
	geom := this.parent.ContentGeometry()
	geom.Height -= this.spacing * (len(children) - 1)

	if len(children) == 0 {
		return
	}

	var data SizeConstrainSlice

	minExpSpace := 0
	fixedSpace := 0
	for i := 0; i < len(children); i++ {
		switch children[i].SizePolicy().Vertical {
		case MinExpand:
			minExpSpace += children[i].MinSize().Height
		case Fixed:
			fixedSpace += children[i].PreferedSize().Height
		}
	}

	distributionData, restSpace := distributeWidgets(children, false, Fixed, geom.Height)
	data = append(data, distributionData...)

	distributionData, restSpace = distributeWidgets(children, false, Expand, restSpace-minExpSpace)
	data = append(data, distributionData...)

	distributionData, restSpace = distributeWidgets(children, false, MinExpand, restSpace)
	data = append(data, distributionData...)

	top := geom.Top

	for i := 0; i < len(children); i++ {
		if !children[i].Visible() {
			continue
		}

		d := data.WidgetData(children[i])
		widgetGeom := children[i].Geometry()

		widgetGeom.Top = top
		widgetGeom.Height = d.size

		switch children[i].SizePolicy().Horizontal {
		case Expand, MinExpand:
			widgetGeom.Width = geom.Width
		}

		//alignment
		switch this.align {
		case Left:
			widgetGeom.Left = geom.Left
		case Right:
			widgetGeom.Left = geom.Left + geom.Width - widgetGeom.Width
		case Center:
			widgetGeom.Left = geom.Left + (geom.Width-widgetGeom.Width)/2
		}

		top += widgetGeom.Height + this.spacing

		children[i].SetGeometry(widgetGeom)
	}
}

func (this *RowLayout) MinSize() (size Size) {
	if this.parent == nil {
		return
	}

	//elements
	children := this.parent.Children()
	for _, e := range children {
		size.Height += e.MinSize().Height
		size.Width = max(size.Width, e.Geometry().Width)
	}

	//spacing and margin
	size.Height += this.spacing*len(children) - 1

	return
}

func (this *RowLayout) Marshal() map[string]interface{} {
	return map[string]interface{}{
		"Spacing": this.spacing,
		"Align":   this.align,
	}
}

func (this *RowLayout) Unmarshal(data JsonRawMap) {
	Unmarshal(data["Spacing"], &this.spacing)
	Unmarshal(data["Align"], &this.align)
}
