// Copyright (C) 2014 by krepa098. All rights reserved.
// Use of this source code is governed by a zlib-style
// license that can be found in the license.txt file.

package acropolis

import "regexp"

//=======================
//		STRUCTS
//=======================

type WidgetContainerBase struct {
	WidgetBase
	children []Widgeter
}

var _ WidgetContainer = &WidgetContainerBase{}

//=======================
//		FUNCS
//=======================

func (this *WidgetContainerBase) Children() []Widgeter {
	return this.children
}

func (this *WidgetContainerBase) FindChildren(regex string, recursive bool) (out []Widgeter) {
	if regex == "" {
		return
	}

	for _, e := range this.children {
		if matches, _ := regexp.MatchString(regex, e.Name()); matches {
			out = append(out, e)
		}
		if container, ok := e.(Container); ok && recursive {
			out = append(out, container.FindChildren(regex, recursive)...)
		}
	}
	return
}

func (this *WidgetContainerBase) AddChild(child Widgeter) {
	for _, w := range this.Children() {
		if w == child {
			return
		}
	}

	this.children = append(this.children, child)
}

func (this *WidgetContainerBase) Update() {

}

func (this *WidgetContainerBase) Marshal() map[string]interface{} {
	m := this.WidgetBase.Marshal()
	m["Children"] = Marshal(this.children)

	return m
}

func (this *WidgetContainerBase) Unmarshal(data JsonRawMap) {
	this.WidgetBase.Unmarshal(data)

	children := UnmarshalArray(data["Children"])
	for i := 0; i < len(children); i++ {
		if _, ok := children[i].(Widgeter); ok {
			this.AddChild(children[i].(Widgeter))
		}
	}
}

func (this *WidgetContainerBase) ContentGeometry() Geometry {
	return this.geometry
}
